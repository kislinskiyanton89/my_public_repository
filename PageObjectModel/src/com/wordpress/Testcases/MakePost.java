package com.wordpress.Testcases;

import Helper.BrowserFactory;
import com.wordpress.Pages.IndexPage;
import com.wordpress.Pages.LoginPageNew;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by kislinskiy.a on 05.05.2016.
 */
public class MakePost {


    @Test
    public void checkValidUser(){

        //this will launch browser and specific url
        WebDriver driver = BrowserFactory.startBrowser("firefox","http://demosite.center/wordpress/wp-login.php");

        //Create Page Object using Page Factory
        LoginPageNew login_page = PageFactory.initElements(driver, LoginPageNew.class);

        //Call the method
        login_page.login_wordpress("admin","demo123");

    }

   @Test
    public void startPostPage(){
       IndexPage open_post_page = PageFactory.initElements(driver, IndexPage.class);
       open_post_page.openPostPage();
       /*              Trouble place          */
   }


}
