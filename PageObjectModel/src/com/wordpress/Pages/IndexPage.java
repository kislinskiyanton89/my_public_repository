package com.wordpress.Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by kislinskiy.a on 05.05.2016.
 */
public class IndexPage {

    WebDriver driver;
    public IndexPage(WebDriver ldriver){
        this.driver=ldriver;

    }

    @FindBy(how = How.XPATH,using = ".//*[@id='menu-posts']/a")
    WebElement post_link;
    @FindBy(how =How.XPATH,using =".//li/a[@href='post-new.php']" ) WebElement add_new_button;



    public void openPostPage(){
        post_link.click();
        add_new_button.click();
    }

}
